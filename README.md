# libsodium

## 简介
> libsodium主要是易用，可移植的加解密库。

![](screenshot/result.png)

## 下载安装
直接在OpenHarmony-SIG仓中搜索libsodium并下载。

## 使用说明
以OpenHarmony 3.1Beta的rk3568版本为例

1. 库代码存放路径：./third_party/libsodium

2. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```
  {
     "subsystem": "developtools",
     "parts": {
       "bytrace_standard": {
         "module_list": [
           "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
           "//developtools/bytrace_standard/bin:bytrace_target",
           "//developtools/bytrace_standard/bin:bytrace.cfg",
           "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
           "//third_party/libsodium:libsodium",
           "//third_party/libsodium:libsodium_test"
         ],
         "inner_kits": [
           {
             "type": "so",
             "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
             "header": {
               "header_files": [
                 "bytrace.h"
               ],
               "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
             }
           }
         ],
          "test_list": [
           "//developtools/bytrace_standard/bin/test:unittest"
         ]
       }
     }
   }
```

3. 用命令 ./build.sh --product-name rk3568 --ccache 编译

4. 生成库文件路径：out/rk3568/graphic/graphic_standard，该路径会生成库文件

## 接口说明

1. 初始化库
   `sodium_init(void)`
   
2. 等式的恒定时间检验
   `sodium_memcmp(const void * const b1_, const void * const b2_, size_t len);`

3. 十六进制编码/解码
   `sodium_bin2hex(char * const hex, const size_t hex_maxlen, const unsigned char * const bin, const size_t bin_len);`

4. Base64编码/解码
   `sodium_bin2base64(char * const b64, const size_t b64_maxlen, const unsigned char * const bin, const size_t bin_len, const int variant)`

5. 调零记忆
   `sodium_memzero(void * const pnt, const size_t len)`

6. 锁定内存
   `int sodium_mlock(void * const addr, const size_t len)`

7. 密钥派生
   `crypto_pwhash(unsigned char * const out, unsigned long long outlen, const char * const passwd, unsigned long long passwdlen, const unsigned char * const salt, unsigned long long opslimit, size_t memlimit, int alg)`

8. 密码存储
   `crypto_pwhash_str(char out[crypto_pwhash_STRBYTES], const char * const passwd, unsigned long long passwdlen, unsigned long long opslimit, size_t memlimit)`

9. 创建一个新的密钥对
   `int sodium_mlock(void * const addr, const size_t len)`

10. 计算共享密钥
   `crypto_kx_client_session_keys(unsigned char rx[crypto_kx_SESSIONKEYBYTES], unsigned char tx[crypto_kx_SESSIONKEYBYTES], const unsigned char client_pk[crypto_kx_PUBLICKEYBYTES], const unsigned char client_sk[crypto_kx_SECRETKEYBYTES], const unsigned char server_pk[crypto_kx_PUBLICKEYBYTES])`

## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- libsodium
|     |---- src
|           |---- crypto_aead   #aead结构
|           |---- crypto_auth   #密钥消息验证
|           |---- crypto_box   #密封盒
|           |---- crypto_core   #有限域算术
|           |---- crypto_generichash   #散列
|           |---- crypto_hash   #哈希
|           |---- crypto_kdf   #密钥派生
|           |---- crypto_kx   #密钥交换
|           |---- crypto_onetimeauth   #一次性认证
|           |---- crypto_pwhash   #密码哈希
|           |---- crypto_scalarmult   #加密标量乘法
|           |---- crypto_secretbox   #认证加密
|           |---- crypto_secretstream   #加密流和文件加密
|           |---- crypto_shorthash   #端输入散列
|           |---- crypto_sign   #公钥签名
|           |---- crypto_stream   #流密码
|           |---- crypto_verify   #公钥签名
|           |---- include   #头文件
|           |---- randombytes   #生成随机数据
|           |---- sodium   #
|     |---- tests   #单元测试用例
|     |---- README.md   #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/libsodium/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/libsodium/pulls) 。

## 开源协议
本项目基于 [ISC License](https://gitee.com/openharmony-sig/libsodium/blob/master/LICENSE) ，请自由地享受和参与开源。

